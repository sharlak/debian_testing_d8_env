#!/bin/bash

echo "Provisioning virtual machine..."
echo "Updating composer"
sudo /usr/bin/composer self-update
echo "Updating composer packages"
composer global update
echo "Downloading Drupal 8..."
drush dl drupal-8
version=$(ls | grep drupal-8)
cd $version
echo "Installing drupal..."
drush site-install -y standard --account-name=root --account-pass=root --db-url=mysql://root:root@localhost/drupal8 --notify=global
cd /var/www/html/
sudo ln -s /home/vagrant/$version drupal8
echo "Setting up apache2..."
sudo chown -R www-data:www-data /home/vagrant/$version
sudo sed -i '/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride all/' /etc/apache2/apache2.conf
echo "Restarting apache2..."
sudo service apache2 restart
echo "Provisioning finished, go to: http://192.168.33.10/drupal8"
echo ""
echo "Do not ever use this configuration in a production site ;-)"
