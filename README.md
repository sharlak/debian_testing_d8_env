# README #
This repo contains a Vagrantfile for a [Debian Testing environment](https://atlas.hashicorp.com/sharlak/boxes/debian_testing_d8_env) and a provision script to install Drupal 8.
## Contents ##
* Dependencies
* Setup
	* Getting the virtual machine and running Drupal 8
* Profit
* Provision script
	* Installing Drupal 8
	* Configuring Apache (fast and dirty)
* VM details: Debian testing Drupal 8 env
	* Credentials
	* Software installed
	* Network configuration
* Special thanks
## Dependencies ##
You will need [Vagrant](http://www.vagrantup.com/downloads.html) and [VirtualBox](http://www.oracle.com/technetwork/server-storage/virtualbox/downloads/index.html) (or use your GNU/Linux distribution's package manager ;-).

Versions tested:

* Vagrant: 1.7.2
* Virtualbox: 4.3.28
## Setup ##
### Getting the virtual machine and running Drupal 8 ###
Clone this repo and run the virtual machine (the box will be downloaded from Vagrant Cloud: [sharlak/debian_testing_d8_env](https://atlas.hashicorp.com/sharlak/boxes/debian_testing_d8_env)):
```
$ git clone https://sharlak@bitbucket.org/sharlak/debian_testing_d8_env.git
$ cd debian_testing_d8_env
$ vagrant up
```
## Profit ##
Vagrant will provision your machine automagically with a Drupal 8 installation, so no further steps are needed. You can see the provision script section below to know what it is doing.

Go to your fresh installed Drupal 8 site in your browser:
```
http://192.168.33.10/drupal8
```
or
```
http://localhost:8000/drupal8
```
Admin credentials: root/root
## Provision script ##
### Updating composer ###
Run composer self update:
```
sudo /usr/bin/composer self-update
```
### Updating composer packages ###
Run composer global update command to update drush and other composer installed packages:
```
composer global update
```
### Installing Drupal 8 ###
Download Drupal 8:
```
$ drush dl drupal-8
```
Go into the downloaded folder and install the site:
```
$ version=$(ls | grep drupal-8)
$ cd $version
$ drush site-install -y standard --account-name=root --account-pass=root --db-url=mysql://root:root@localhost/drupal8
```
### Configuring Apache (fast and dirty) ###
Create a symlink in the apache default folder:
```
$ cd /var/www/html/
$ sudo ln -s /home/vagrant/$version drupal8
```
Give permissions to the apache user:
```
$ sudo chown -R www-data:www-data /home/vagrant/$version
```
Change ```AllowOverride None``` for ```AllowOverride all``` in the apache default config file for the default folder ```/var/www/```:
```
sudo sed -i '/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride all/' /etc/apache2/apache2.conf
```
Restart apache:
```
sudo service apache2 restart
```
## VM details: Debian testing Drupal 8 env##
**Do not ever use this configuration in a production site ;-)**
### Credentials ###
* Machine: vagrant/vagrant (added to sudoers with NOPASSWD:ALL)
* MySQL: root/root
* Drupal: root/root
### Software installed ###
* Debian Testing
* Apache 2 (+ mod_rewrite)
* MySQL server
* PHP5 (+ connectors + php-gd)
* Drush
* Sudo
* Git
* Vim
* VirtualBox Guest Additions
* Drupal 8
### Network configuration ###
* Added network adapter: 192.168.33.10
* Ports forwarded: 
    * ssh: 2222 -> 22
    * http: 8000 -> 80

The network adapter and the http port forward are configured in the Vagrantfile.
## Special thanks ##
* Special thanks to [Tyler Bird](https://blog.engineyard.com/authors/Tyler%20Bird) for his [great blog post about creating Vagrant boxes](https://blog.engineyard.com/2014/building-a-vagrant-box) :-)
* Thanks to [keopx](http://www.keopx.net/) ([in bitbucket](https://bitbucket.org/keopx)) for contributing the provision script
